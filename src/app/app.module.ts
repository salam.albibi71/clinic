import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/services/shared.module';
import { HomepageModule } from './homepage/services/homepage.module';
import { DentalModule } from './dental/services/dental.module';
import { OrthodontistsModule } from './orthodontists/services/orthodontists.module';
import { CosmeticDentistryModule } from './cosmeticDentistry/services/cosmetic-dentistry.module';

import { DentalAppliancesModule } from './dentalAppliances/services/dental-appliances.module';
import { GeneralDentistryModule } from './generalDentistry/services/general-dentistry.module';


@NgModule({
  declarations: [AppComponent, ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    HomepageModule,
    DentalModule,
    OrthodontistsModule,
    CosmeticDentistryModule,
    DentalAppliancesModule,
    GeneralDentistryModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
