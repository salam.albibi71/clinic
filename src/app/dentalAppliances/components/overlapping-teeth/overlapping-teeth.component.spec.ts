import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlappingTeethComponent } from './overlapping-teeth.component';

describe('OverlappingTeethComponent', () => {
  let component: OverlappingTeethComponent;
  let fixture: ComponentFixture<OverlappingTeethComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OverlappingTeethComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OverlappingTeethComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
