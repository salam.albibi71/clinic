import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClearAlignerComponent } from './clear-aligner.component';

describe('ClearAlignerComponent', () => {
  let component: ClearAlignerComponent;
  let fixture: ComponentFixture<ClearAlignerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClearAlignerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ClearAlignerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
