import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NightGuardComponent } from './night-guard.component';

describe('NightGuardComponent', () => {
  let component: NightGuardComponent;
  let fixture: ComponentFixture<NightGuardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NightGuardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NightGuardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
