import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClearAlignerComponent } from '../components/clear-aligner/clear-aligner.component';
import { NightGuardComponent } from '../components/night-guard/night-guard.component';
import { OverlappingTeethComponent } from '../components/overlapping-teeth/overlapping-teeth.component';
import { SharedModule } from 'src/app/shared/services/shared.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ClearAlignerComponent,
    NightGuardComponent,
    OverlappingTeethComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class DentalAppliancesModule {}
