import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DentalveneersComponent } from '../components/dentalveneers/dentalveneers.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/services/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DigitalSmileComponent } from '../components/digital-smile/digital-smile.component';
import { ZirconiaCrownComponent } from '../components/zirconia-crown/zirconia-crown.component';



@NgModule({
  declarations: [
    DentalveneersComponent,
    DigitalSmileComponent,
    ZirconiaCrownComponent,

  ],
  imports: [
    CommonModule,
     SharedModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class CosmeticDentistryModule { }
