import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZirconiaCrownComponent } from './zirconia-crown.component';

describe('ZirconiaCrownComponent', () => {
  let component: ZirconiaCrownComponent;
  let fixture: ComponentFixture<ZirconiaCrownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZirconiaCrownComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ZirconiaCrownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
