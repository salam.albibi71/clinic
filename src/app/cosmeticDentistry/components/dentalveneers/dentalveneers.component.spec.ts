import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DentalveneersComponent } from './dentalveneers.component';

describe('DentalveneersComponent', () => {
  let component: DentalveneersComponent;
  let fixture: ComponentFixture<DentalveneersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DentalveneersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DentalveneersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
