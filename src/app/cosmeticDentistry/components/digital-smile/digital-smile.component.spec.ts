import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DigitalSmileComponent } from './digital-smile.component';

describe('DigitalSmileComponent', () => {
  let component: DigitalSmileComponent;
  let fixture: ComponentFixture<DigitalSmileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DigitalSmileComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DigitalSmileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
