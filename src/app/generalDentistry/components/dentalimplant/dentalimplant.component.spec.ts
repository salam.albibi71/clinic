import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DentalimplantComponent } from './dentalimplant.component';

describe('DentalimplantComponent', () => {
  let component: DentalimplantComponent;
  let fixture: ComponentFixture<DentalimplantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DentalimplantComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DentalimplantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
