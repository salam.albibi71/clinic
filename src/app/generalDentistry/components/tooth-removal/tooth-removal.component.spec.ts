import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToothRemovalComponent } from './tooth-removal.component';

describe('ToothRemovalComponent', () => {
  let component: ToothRemovalComponent;
  let fixture: ComponentFixture<ToothRemovalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToothRemovalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ToothRemovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
