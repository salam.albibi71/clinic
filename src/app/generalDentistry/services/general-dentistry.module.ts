import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/services/shared.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DentalimplantComponent } from '../components/dentalimplant/dentalimplant.component';
import { PediatricDentistryComponent } from '../components/pediatric-dentistry/pediatric-dentistry.component';
import { ToothRemovalComponent } from '../components/tooth-removal/tooth-removal.component';



@NgModule({
  declarations: [DentalimplantComponent, PediatricDentistryComponent,
  ToothRemovalComponent],
  imports: [
    CommonModule,
    SharedModule,
      RouterModule,
    HttpClientModule,
    FormsModule,

  ]

})
export class GeneralDentistryModule { }
