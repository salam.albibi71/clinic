import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DentalComponent } from '../components/dental/dental.component';
import { SharedModule } from 'src/app/shared/services/shared.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [DentalComponent, ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class DentalModule {}
