import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeethwhiteningComponent } from './teethwhitening.component';

describe('TeethwhiteningComponent', () => {
  let component: TeethwhiteningComponent;
  let fixture: ComponentFixture<TeethwhiteningComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeethwhiteningComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TeethwhiteningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
