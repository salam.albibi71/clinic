import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrookedteethComponent } from './crookedteeth.component';

describe('CrookedteethComponent', () => {
  let component: CrookedteethComponent;
  let fixture: ComponentFixture<CrookedteethComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrookedteethComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CrookedteethComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
