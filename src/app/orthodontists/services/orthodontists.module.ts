import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeethwhiteningComponent } from '../components/teethwhitening/teethwhitening.component';
import { SharedModule } from 'src/app/shared/services/shared.module';
import { RootcanalComponent } from '../components/rootcanal/rootcanal.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CrookedteethComponent } from '../components/crookedteeth/crookedteeth.component';



@NgModule({
  declarations: [
    TeethwhiteningComponent,
CrookedteethComponent,
    RootcanalComponent],
  imports: [
    CommonModule,
    SharedModule,
      RouterModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class OrthodontistsModule { }
