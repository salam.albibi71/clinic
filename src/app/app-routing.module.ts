import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './homepage/components/homepage/homepage.component';
import { DentalComponent } from './dental/components/dental/dental.component';
import { DentalimplantComponent } from './generalDentistry/components/dentalimplant/dentalimplant.component';
import { RootcanalComponent } from './orthodontists/components/rootcanal/rootcanal.component';
import { CrookedteethComponent } from './orthodontists/components/crookedteeth/crookedteeth.component';
import { TeethwhiteningComponent } from './orthodontists/components/teethwhitening/teethwhitening.component';
import { ZirconiaCrownComponent } from './cosmeticDentistry/components/zirconia-crown/zirconia-crown.component';
import { DigitalSmileComponent } from './cosmeticDentistry/components/digital-smile/digital-smile.component';
import { DentalveneersComponent } from './cosmeticDentistry/components/dentalveneers/dentalveneers.component';
import { ClearAlignerComponent } from './dentalAppliances/components/clear-aligner/clear-aligner.component';
import { OverlappingTeethComponent } from './dentalAppliances/components/overlapping-teeth/overlapping-teeth.component';
import { NightGuardComponent } from './dentalAppliances/components/night-guard/night-guard.component';
import { PediatricDentistryComponent } from './generalDentistry/components/pediatric-dentistry/pediatric-dentistry.component';
import { ToothRemovalComponent } from './generalDentistry/components/tooth-removal/tooth-removal.component';

const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'homepage', component: HomepageComponent },
  { path: 'dental', component: DentalComponent },
  { path: 'dentalimplant', component: DentalimplantComponent },
    { path: 'rootcanal', component: RootcanalComponent },
    { path: 'crookedteethtreatment', component: CrookedteethComponent },
  { path: 'teethwhitening', component: TeethwhiteningComponent },
      { path: 'digitalsmile', component: DigitalSmileComponent },
  { path: 'dentalveneers', component: DentalveneersComponent },
  { path: 'zirconiacrown', component: ZirconiaCrownComponent },
   { path: 'clearaligner', component: ClearAlignerComponent },
  { path: 'overlappingteeth', component: OverlappingTeethComponent },
  { path: 'nightguard', component: NightGuardComponent },
      { path: 'pediatricdentistry', component: PediatricDentistryComponent },
      { path: 'toothremoval', component: ToothRemovalComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
